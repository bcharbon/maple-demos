# Maple tutorials

## Description
I find Maple to be a useful tool to
- verify the computations of my students when I read their work,
- verify computations in papers I read,
- compare conventions used by different authors in describing certain mathematical objects
- do mathematical research.
I have thus demoed Maple to various audiences, and provide here the files I used.

## Warning
Despite having used Maple since 1996, I do not think I am a Maple expert. There are likely flaws in how I coded things, and there are likely better ways to code things I have not implemented. I have strived however to make those files useful to your own journey in learning how to use Maple, and humbly hope you will use those files to help your own growth.

## Acknowledgment
Benoit Charbonneau acknowledges the support of the Natural Sciences and Engineering Research Council of Canada (NSERC), RGPIN-2019-04375.

